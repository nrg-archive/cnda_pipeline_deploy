#!/usr/bin/python

# import re
import sys
import argparse

versionNumber="2"
dateString="2014/02/17 12:11:00"
author="flavin"
progName=sys.argv[0].split('/')[-1]
idstring = "$Id: %s,v %s %s %s Exp $"%(progName,versionNumber,dateString,author)

#######################################################
# PARSE INPUT ARGS
parser = argparse.ArgumentParser(description="Generate param file for PUP processing from bash-formatted pipeline params")
parser.add_argument("-v", "--version",
                    help="Print version number and exit",
                    action="version",
                    version=versionNumber)
parser.add_argument("--idstring",
                    help="Print id string and exit",
                    action="version",
                    version=idstring)
parser.add_argument("bashParamFilePath",
                    help="Path to bash-formatted pipeline params file")
parser.add_argument("halflifeCSVPath",
                    help="Path to file mapping tracer -> half life")
parser.add_argument("filterCSVPath",
                    help="Path to file mapping scanner -> filterxy/filterz")
parser.add_argument("PUPParamFilePath",
                    help="Path to PUP-formatted params file (output)")
args=parser.parse_args()
#######################################################

# def getDictFromKeyList(dictionary,keyList):
#     return dict(zip(keyList,map(dictionary.get,keyList)))

def paramDictString(d,paramSep,lineSep):
    return lineSep.join( [paramSep.join(kvtuple) for kvtuple in d.iteritems()] )

def addParamList(l):
    d={k:bashParamDict[k] for k in l}
    print paramDictString(d,'=','\n')
    pupParamDict.update(d)

#######################################################

print idstring

# Parse the bash params file. Build a params dictionary.
bashParamDict = {}
print ''
print 'Reading params from file %s'%args.bashParamFilePath
with open(args.bashParamFilePath,'r') as bashParamFile:
    strippedLines = (line.strip('\n') for line in bashParamFile if line[0] != '#' and '=' in line)
    for line in strippedLines:
        [bashParamName,bashParamVal]=line.split('=')
        # bashParamDict[bashParamName]=bashParamVal.strip('"')
        bashParamDict[bashParamName]=bashParamVal

# genReqArgs=['petdir','petid','half_life','format','delay','tbl','tolmoco','tolreg','rmf','mmf','mst','mdt','suvr','fwhm','refimg']
# quotedReqArgs=['petfn','rbf','mbf','modf']
genReqArgs=['petdir','petid','half_life','format','delay','tbl','tolmoco','tolreg','rmf','mmf','mst','mdt','suvr','fwhm','refimg','petfn','rbf','mbf','modf','refroistr','FS','roiimg','roilist']
roiFSArgs=['fsdir','t1','wmparc','fslut','rsfflag','rsflist','pvc2cflag']
roiTemplateArgs=['tgflag','petatldir','maskroot']
# roiManualArgs=['tgflag','mrdir','mrid','atltarg','atlpath','roidir']
# roiManualQuotedArgs=['mrfn']
roiManualArgs=['tgflag','mrdir','mrid','atltarg','atlpath','roidir','mrfn']
modelArgs=['model','k2']
# if filtering is on, set filterxy,filterz from look-up table, set fwhm=8.0

try:
    tracer=bashParamDict['tracer']
    half_life=''
    print ''
    print 'Looking up half life for tracer %s in file %s'%(tracer,args.halflifeCSVPath)
    try:
        with open(args.halflifeCSVPath,'r') as halflifeCSV:
            strippedLines = (line.strip('\n') for line in halflifeCSV if line[0] != '#' and ',' in line)
            for line in strippedLines:
                tr,hl=line.split(',')
                if tracer.lower() == tr.lower():
                    print 'Found tracer %s'%tr
                    print 'Found half life %s'%hl
                    if half_life=='':
                        half_life=hl
                    elif half_life==hl:
                        print 'Identical to previously found value'
                    else:
                        sys.exit('%s ERROR: multiple matches found for tracer %s in file %s'%(progName,tracer,args.halflifeCSVPath))
    except IOError:
        sys.exit('%s ERROR: Could not open file %s'%(progName,args.halflifeCSVPath))
    if half_life != '':
        bashParamDict['half_life']=half_life
    else:
        sys.exit('%s ERROR: no half life found for tracer %s in file %s'%(progName,tracer,args.halflifeCSVPath))

    print ''
    print 'Do filtering? filter=%s'%bashParamDict['filter']
    if bashParamDict['filter']=='1':
        addFilter=True
        scanner=bashParamDict['scanner']
        filterxy,filterz='',''
        print 'Looking up scanner %s in file %s'%(scanner,args.filterCSVPath)
        try:
            with open(args.filterCSVPath,'r') as filterCSV:
                strippedLines = (line.strip('\n') for line in filterCSV if line[0] != '#' and ',' in line)
                for line in strippedLines:
                    sc,fxy,fz=line.split(',')
                    if scanner.lower() == sc.lower():
                        print 'Found scanner %s'%sc
                        print 'Found filterxy %s, filterz %s'%(fxy,fz)
                        if filterxy=='' and filterz=='':
                            filterxy=fxy
                            filterz=fz
                        elif filterxy==fxy and filterz==fz:
                            print 'Identical to previously found values'
                        else:
                            sys.exit('%s ERROR: multiple matches found for scanner %s in file %s'%(progName,scanner,args.filterCSVPath))
        except IOError:
            sys.exit('%s ERROR: Could not open file %s'%(progName,args.filterCSVPath))
        if filterxy == '' or filterz == '':
            sys.exit('%s ERROR: no match found for scanner %s in file %s'%(progName,scanner,args.filterCSVPath))
    else:
        print 'Not looking up filterxy, filterz.'


    pupParamDict = {}
    print ''
    print 'Building param dictionary'
    print 'Adding required params'
    addParamList(genReqArgs)
    print ''
    # pupParamDict.update( {k:'"%s"'%bashParamDict[k] for k in quotedReqArgs} )
    # Add roi req args
    if bashParamDict.get('FS')=='1':
        print 'Param FS=%s. Adding FS roi args'%bashParamDict.get('FS')
        addParamList(roiFSArgs)
    elif bashParamDict.get('tgflag')=='1':
        print 'Param tgflag=%s. Adding template roi args'%bashParamDict.get('tgflag')
        addParamList(roiTemplateArgs)
    elif bashParamDict.get('tgflag')=='2':
        print 'Param tgflag=%s. Adding manual roi args'%bashParamDict.get('tgflag')
        addParamList(roiManualArgs)
        # pupParamDict.update( {k:'"%s"'%bashParamDict[k] for k in roiManualQuotedArgs} )
    else:
        sys.exit("%s ERROR: FS=%s, tgflag=%s. Must have FS=1 or tgflag=1 or tgflag=2"%(progName,bashParamDict['FS'],bashParamDict['tgflag']))

    if bashParamDict.get('delay')=='0' and bashParamDict.get('tgflag')!='1':
        print ''
        print 'delay=%s and tgflag=%s. Adding model args'%(bashParamDict.get('delay'),bashParamDict.get('tgflag'))
        addParamList(modelArgs)
    else:
        print ''
        print 'delay=%s and tgflag=%s. Not adding model args'

    if addFilter:
        print ''
        print 'filter=%s. Adding filterxy, filterz'%bashParamDict['filter']

        print 'filterxy='+filterxy
        pupParamDict['filterxy']=filterxy
        print 'filterz='+filterz
        pupParamDict['filterz']=filterz
    else:
        print ''
        print 'filter=%s. Not adding filterxy, filterz'%bashParamDict['filter']

except KeyError,key:
    sys.exit('%s ERROR: Required parameter %s is not defined in bash param file %s'%(progName,key,args.bashParamFilePath))


pupParamString = paramDictString(pupParamDict,'=','\n\n')
print ''
print 'WRITING PARAM FILE %s'%args.PUPParamFilePath
with open(args.PUPParamFilePath,'w') as pupParamFile:
    pupParamFile.write(pupParamString)

