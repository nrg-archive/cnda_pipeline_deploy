#!/bin/bash -e

VERSION=1.0
if [ "$1" == "--version" ]; then
    echo $VERSION
    exit 0
fi

author="flavin"
program=`basename $0`
prog_date="20140402_1201"
idstr='$Id: '$program',v '$VERSION' '$prog_date' '$author' $'
echo $idstr

paramsfile=$1
source $paramsfile

cd QC
if [ -e qc.xml ]; then
    rm qc.xml
fi
touch qc.xml

# set date = `date "+%Y-%m-%d"`
nirun=${#irun[*]}
date=`date "+%Y-%m-%d"`
# echo $date
echo '<?xml version="1.0" encoding="UTF-8"?>' >> qc.xml


echo '<xnat:QCAssessment ID="'$reconId'" type="WUSTL_4dfp_BOLD_QC" project="'$project'" label="'$reconLabel'" xsi:schemaLocation="http://nrg.wustl.edu/xnat xnat.xsd"' >> qc.xml
echo 'xmlns:xnat="http://nrg.wustl.edu/xnat"' >> qc.xml
echo 'xmlns:prov="http://www.nbrin.net/prov" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' >>qc.xml
echo '<xnat:date>'$date'</xnat:date>' >> qc.xml

echo '<xnat:imageSession_ID>'$xnat_id'</xnat:imageSession_ID>' >> qc.xml

mpr_to_taget=`cat eta_mpr_to_target.txt`
anatave_to_t2=`cat eta_anatave_to_t2.txt`
t2_to_mpr=`cat eta_t2_to_mpr.txt`
echo '<xnat:parameters>' >> qc.xml
echo '<xnat:addParam name="ETA_MPR_to_TARGET">'$mpr_to_taget'</xnat:addParam>' >> qc.xml
echo '<xnat:addParam name="ETA_ANATAVE_to_T2">'$anatave_to_t2'</xnat:addParam>' >> qc.xml
echo '<xnat:addParam name="ETA_T2_to_MPR">'$t2_to_mpr'</xnat:addParam>' >> qc.xml
echo '<xnat:addParam name="RECONID">'$reconId'</xnat:addParam>' >> qc.xml
echo '</xnat:parameters>' >> qc.xml

echo '<xnat:scans>' >> qc.xml

for (( j=0; j < nirun; j++ )); do
    mov=`cat mov${irun[$j]}.txt`
    mov_ddat=`cat ddat_mov${irun[$j]}.txt`
    sd=`cat sd_bold${irun[$j]}.txt`

    echo '<xnat:scan id="'${fstd[$j]}'">' >> qc.xml
    echo '<xnat:scanStatistics xsi:type="xnat:statisticsData">' >> qc.xml
    echo '<xnat:additionalStatistics name="RMS_ERROR">'$mov'</xnat:additionalStatistics>' >> qc.xml
    echo '<xnat:additionalStatistics name="RMS_ddat">'$mov_ddat'</xnat:additionalStatistics>' >> qc.xml
    echo '<xnat:additionalStatistics name="SD">'$sd'</xnat:additionalStatistics>' >> qc.xml
    echo '</xnat:scanStatistics>' >> qc.xml
    echo '</xnat:scan>' >> qc.xml

done

echo '</xnat:scans>' >> qc.xml
echo '</xnat:QCAssessment>' >> qc.xml

exit 0
