#!/bin/bash

source /nrgpackages/scripts/epd-python_setup.sh

export FREESURFER_HOME=/nrgpackages/tools/freesurfer5

PUP_DIR=/nrgpackages/tools/PUP

export PATH=$PUP_DIR/bin:$PUP_DIR/scripts:`sed 's#/nrgpackages/tools/nil-tools#/nrgpackages/tools.release/lin64-nil-tools-20140730#' <<< $PATH`
