#!/bin/bash

VERSION=2.0
if [ "$1" == "--version" ]; then
    echo $VERSION
    exit 0
fi

INPUT_DIR=$1
OUTPUT_DIR=$2

IS_SECONDARY=false

## FIRST, pull headers from dicom files
HEADERS=`find $INPUT_DIR -iname *.dcm -o -iname *.ima | head -1 | xargs /nrgpackages/tools/nil-tools/dcm_dump_file`

## SOME secondary scans have been put in non-standard subdirectories, so we'll automatically exclude those
if [ $? -ne 0 ] ; then
   IS_SECONDARY=true
fi

## THEN, identify characteristics indicating secondary captures

## CHECK 1: Check modality (SC or SR)

MODALITY=`echo "$HEADERS" | grep -a "0008[ ,]0060" | sed -e 's/^.*\/\/ *//'`

echo "MODALITY=$MODALITY"

if [ "$MODALITY" == "SC" ] ||  [ "$MODALITY" == "SR" ] ; then

   IS_SECONDARY=true

fi

## OUTPUT FILE INDICATOR FOR SECONDARY CAPTURES

if [ "$IS_SECONDARY" == "true" ] ; then

   echo "Scan excluded due to being a secondary capture" >  $OUTPUT_DIR/README_scan_excluded

fi
