#!/bin/bash

if test -z "$3"; then
  echo 1.0
  exit
fi

set +x

proj=$1
visit=$2
subjID=$3
subjLabel=$4
ptNotes=$5
csNotes=$6
user=$7
pass=$8
host=$9

shift 1
zipfile=$9

shift 1
catalogPath=$9

resource_loc=${catalogPath}/ePrime/resources/

workdir=${proj}_${subjID}_${visit}_ipip

mkdir $workdir

mv $zipfile $workdir
cd $workdir

unzip -j -o `basename $zipfile`

for f in IPIP*.txt; do
  if echo "$f" | grep -q CS; then
	note=$csNotes
  else
	note=$ptNotes
  fi

  iconv -f UCS-2LE -t UTF-8 "$f" > iconvOut.txt

  perl ${resource_loc}eprime2xml ${resource_loc}ipip-instructions.txt iconvOut.txt > dukeXML.xml

  xsltproc --stringparam project $proj --stringparam visit_id $visit --stringparam subject_ID $subjID --stringparam subject_Label $subjLabel --stringparam note "$note" ${resource_loc}ipip-format.xsl dukeXML.xml > xnatXML.xml
  
  label=`grep label xnatXML.xml | cut -d'"' -f8`
  restPath=${host}/REST/projects/${proj}/subjects/${subjID}/experiments/${label}
  curl -u ${user}:${pass} -T xnatXML.xml "${restPath}?inbody=true"
  
  basefilename=`basename "$f" .txt`
  zipfilename=$basefilename
  zipfilename=`echo $zipfilename | tr " " "-"`
  zip "${zipfilename}.zip" "${basefilename}.txt" "${basefilename}.edat" "${basefilename}.edat2"
  
  restPath="${restPath}/resources/IPIP/files/${zipfilename}.zip?overwrite=true"
  curl -u ${user}:${pass} -F upload=@${zipfilename}.zip "${restPath}?inbody=true"
done

#rm *

cd -

#rmdir $workdir
