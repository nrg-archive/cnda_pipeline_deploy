#!/bin/bash

#################################################################################################
## NOTE:  This version names files according to the latest file naming convention (2011-12-15) ##
#################################################################################################

if [ $# -lt 8 ] ; then
   echo "1.0"
   exit 0
fi

export USER=$1
export PASSWORD=$2
export HOST=$3
export OUT_DIR=$4
export PROJECT=$5
export SUBJECT=$6
export SESSION=$7
export SCAN=$8
export OVERWRITE_EXISTING=$9

if [ ! -d "$OUT_DIR" ] ; then
   echo "ERROR:  Specified directory does not exist"
   exit 1
fi

cd $OUT_DIR

## CHANGE FOR DEFACED PIPELINE ##
if [ `find . -type f | wc -l` -le 0 ] ; then

   ## Okay here.  Only T1, T2 should have files if this is a renaming run.
   exit 0

fi

export RESOURCE=NIFTI;
if [[ $OUT_DIR == */NIFTI_RAW/* ]] ; then

   RESOURCE=NIFTI_RAW;

fi
echo "RESOURCE=$RESOURCE"
## END CHANGE FOR DEFACED PIPELINE ##


if [ $OVERWRITE_EXISTING == "Y" ] ; then

   if [ `curl -u ${USER}:${PASSWORD} -X GET ${HOST}/data/archive/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/$RESOURCE/files?format=csv | wc -l` -gt 1 ] ; then
      echo "OVERWRITING EXISTING FILES - SCAN ${SCAN}"
      curl -u ${USER}:${PASSWORD} -X DELETE ${HOST}/data/archive/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/$RESOURCE
   fi

else

   if [ `curl -u ${USER}:${PASSWORD} -X GET ${HOST}/data/archive/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/$RESOURCE/files?format=csv | wc -l` -gt 1 ] ; then
      echo "ERROR:  $RESOURCE FILES ALREADY EXIST AND OVERWRITE HAS BEEN SET TO \"N\"."
      exit 1
   fi

fi

## Create $RESOURCE resource if it doesn't exist, setting format and content attributes
curl -u ${USER}:${PASSWORD} -X PUT ${HOST}/data/archive/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/$RESOURCE?format=NIFTI\&content=RAW


##############################################################
## LOOP OVER FILES GENERATING ARCHIVE NAME AND SENDING THEM ##
##############################################################

for FIL in $(find . -type f)
do

   echo "ORIFN=$FIL"

   NEWFN=$FIL

   echo "NEWFN=$NEWFN"

   SENDFN=`basename "$NEWFN"`
   LOCALPATH=`echo "$FIL" | sed "#^.#$(pwd)#"`
   REMOTEPATH="/data/archive/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/$RESOURCE/files/${SENDFN}"

   echo "SENDFN=$SENDFN"

   #################
   ## UPLOAD FILE ##
   #################

   curl -u ${USER}:${PASSWORD} -X PUT -T "$LOCALPATH" ${HOST}${REMOTEPATH}?extract=false\&inbody=true\&format=NIFTI\&content=RAW

done

